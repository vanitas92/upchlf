echo
echo "#############################################################"
echo "######        Golang vendor chaincode degree           ######"
echo "#############################################################"

cd /opt/gopath/src/github.com/chaincodes/degree
go mod download
cd /opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/

echo
echo "#############################################################"
echo "######           Package chaincode degree              ######"
echo "#############################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="generalitatMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/users/Admin@generalitat.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS=peer0.generalitat.uniconsortium.com:7051
CHAINCODEPATH=/opt/gopath/src/github.com/chaincodes/degree
LABEL=degree_1

peer lifecycle chaincode package degree.tar.gz --path $CHAINCODEPATH --lang golang --label $LABEL

echo
echo "#############################################################"
echo "######     Install chaincode degree - Generalitat      ######"
echo "#############################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="generalitatMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/users/Admin@generalitat.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS=peer0.generalitat.uniconsortium.com:7051
CHAINCODEPATH=github.com/chaincodes/degree
VERSION=1.0

peer lifecycle chaincode install degree.tar.gz

echo
echo "#############################################################"
echo "######     Install chaincode degree - ub               ######"
echo "#############################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="ubMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/peers/peer0.ub.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/users/Admin@ub.uniconsortium.com/msp
CORE_PEER_ADDRESS=peer0.ub.uniconsortium.com:7051
CHAINCODENAME=degree
CHAINCODEPATH=github.com/chaincodes/degree
VERSION=1.0

peer lifecycle chaincode install degree.tar.gz

echo
echo "#############################################################"
echo "######     Install chaincode degree - upc              ######"
echo "#############################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="upcMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/peers/peer0.upc.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/users/Admin@upc.uniconsortium.com/msp
CORE_PEER_ADDRESS=peer0.upc.uniconsortium.com:7051
CHAINCODENAME=degree
CHAINCODEPATH=github.com/chaincodes/degree
VERSION=1.0

peer lifecycle chaincode install degree.tar.gz