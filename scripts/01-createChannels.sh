echo
echo "####################################################################"
echo "#######         Create channel commonChannel               #########"
echo "####################################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="upcMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/peers/peer0.upc.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/users/Admin@upc.uniconsortium.com/msp
CORE_PEER_ADDRESS=peer0.upc.uniconsortium.com:7051
CHANNEL_NAME=commonchannel
CORE_PEER_TLS_ENABLED=true
peer channel create -o $ORDERER -c $CHANNEL_NAME -f ./../channel-artifacts/commonChannel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA

echo
echo "####################################################################"
echo "#######         Create channel upcGenChannel               #########"
echo "####################################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="upcMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/peers/peer0.upc.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/users/Admin@upc.uniconsortium.com/msp
CORE_PEER_ADDRESS=peer0.upc.uniconsortium.com:7051
CHANNEL_NAME=upcgenchannel
CORE_PEER_TLS_ENABLED=true
peer channel create -o $ORDERER -c $CHANNEL_NAME -f ./../channel-artifacts/upcGenChannel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA

echo
echo "####################################################################"
echo "#######         Create channel ubGenChannel               #########"
echo "####################################################################"
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="ubMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/peers/peer0.ub.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/users/Admin@ub.uniconsortium.com/msp
CORE_PEER_ADDRESS=peer0.ub.uniconsortium.com:7051
CHANNEL_NAME=ubgenchannel
CORE_PEER_TLS_ENABLED=true
peer channel create -o $ORDERER -c $CHANNEL_NAME -f ./../channel-artifacts/ubGenChannel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA