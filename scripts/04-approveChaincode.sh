echo
echo "#############################################################"
echo "###### Approve degree chaincode UPC for upcGenChannel  ######"
echo "#############################################################"

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="upcMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/peers/peer0.upc.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/users/Admin@upc.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS=peer0.upc.uniconsortium.com:7051
VERSION=1.0
CHANNEL_NAME=upcgenchannel
CORE_PEER_TLS_ENABLED=true
CC_PACKAGE_ID=degree_1:918008fd4abb424ac6476d04c53f7e2dbe09f0a6de80ada2b90bec777598da0a

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name $CHAINCODENAME --version $VERSION --init-required --package-id $CC_PACKAGE_ID --sequence 1 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA

echo
echo "#############################################################"
echo "###### Approve degree chaincode Gen for upcGenChannel  ######"
echo "#############################################################"

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="generalitatMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/users/Admin@generalitat.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS=peer0.generalitat.uniconsortium.com:7051
VERSION=1.0
CHANNEL_NAME=upcgenchannel
CORE_PEER_TLS_ENABLED=true
CC_PACKAGE_ID=degree_1:918008fd4abb424ac6476d04c53f7e2dbe09f0a6de80ada2b90bec777598da0a

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name $CHAINCODENAME --version $VERSION --init-required --package-id $CC_PACKAGE_ID --sequence 1 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA


echo
echo "#############################################################"
echo "###### Approve degree chaincode UB for ubGenChannel   ######"
echo "#############################################################"

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="ubMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/peers/peer0.ub.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/users/Admin@ub.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS=peer0.ub.uniconsortium.com:7051
VERSION=1.0
CHANNEL_NAME=ubgenchannel
CORE_PEER_TLS_ENABLED=true
CC_PACKAGE_ID=degree_1:918008fd4abb424ac6476d04c53f7e2dbe09f0a6de80ada2b90bec777598da0a

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name $CHAINCODENAME --version $VERSION --init-required --package-id $CC_PACKAGE_ID --sequence 1 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA

echo
echo "#############################################################"
echo "###### Approve degree chaincode Gen for ubGenChannel  ######"
echo "#############################################################"

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="generalitatMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/users/Admin@generalitat.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS=peer0.generalitat.uniconsortium.com:7051
VERSION=1.0
CHANNEL_NAME=ubgenchannel
CORE_PEER_TLS_ENABLED=true
CC_PACKAGE_ID=degree_1:918008fd4abb424ac6476d04c53f7e2dbe09f0a6de80ada2b90bec777598da0a

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name $CHAINCODENAME --version $VERSION --init-required --package-id $CC_PACKAGE_ID --sequence 1 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
