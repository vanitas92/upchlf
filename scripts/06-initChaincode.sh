echo
echo "#############################################################"
echo "#######    Init degree chaincode upcGenChannel     ########"
echo "#############################################################"

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="generalitatMSP"
CORE_PEER_TLS_ROOTCERT_FILE_GENERALITAT=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt
CORE_PEER_TLS_ROOTCERT_FILE_UPC=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/peers/peer0.upc.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/users/Admin@generalitat.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS_GENERALITAT=peer0.generalitat.uniconsortium.com:7051
CORE_PEER_ADDRESS_UPC=peer0.upc.uniconsortium.com:7051
VERSION=1.0
CHANNEL_NAME=upcgenchannel
CORE_PEER_TLS_ENABLED=true


peer chaincode invoke --tls true -C $CHANNEL_NAME --name $CHAINCODENAME --cafile $ORDERER_CA --isInit -c '{"Args":[""]}' --peerAddresses $CORE_PEER_ADDRESS_GENERALITAT --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_GENERALITAT --peerAddresses $CORE_PEER_ADDRESS_UPC --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_UPC


echo
echo "#############################################################"
echo "#######    Init degree chaincode ubGenChannel     ## ######"
echo "#############################################################"

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
ORDERER=orderer.uniconsortium.com:7050
CORE_PEER_LOCALMSPID="generalitatMSP"
CORE_PEER_TLS_ROOTCERT_FILE_GENERALITAT=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt
CORE_PEER_TLS_ROOTCERT_FILE_UB=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ub.uniconsortium.com/peers/peer0.ub.uniconsortium.com/tls/ca.crt
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/users/Admin@generalitat.uniconsortium.com/msp
CHAINCODENAME=degree
CORE_PEER_ADDRESS_GENERALITAT=peer0.generalitat.uniconsortium.com:7051
CORE_PEER_ADDRESS_UB=peer0.ub.uniconsortium.com:7051
VERSION=1.0
CHANNEL_NAME=ubgenchannel
CORE_PEER_TLS_ENABLED=true

peer chaincode invoke --tls true -C $CHANNEL_NAME --name $CHAINCODENAME --cafile $ORDERER_CA --isInit -c '{"Args":[""]}' --peerAddresses $CORE_PEER_ADDRESS_GENERALITAT --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_GENERALITAT --peerAddresses $CORE_PEER_ADDRESS_UB --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_UB