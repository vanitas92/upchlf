#!/bin/bash

set -e

PROJECT_DIR=$PWD

ARGS_NUMBER="$#"
COMMAND="$1"

function verifyArg() {

    if [ $ARGS_NUMBER -ne 1 ]; then
        echo "Usage: fabricOps.sh start | status | clean | cli | peer"
        exit 1;
    fi
}

function pullDockerImages(){
  local FABRIC_TAG="amd64-2.1.0"
  for IMAGES in peer orderer ccenv tools; do
      echo "==> FABRIC IMAGE: $IMAGES"
      echo
      docker pull hyperledger/fabric-$IMAGES:$FABRIC_TAG
  done
}

function replacePrivateKey () {

    echo # Replace key

        # ARCH=`uname -s | grep Darwin`
        # if [ "$ARCH" == "Darwin" ]; then
        #       OPTS="-it"
        # else
                OPTS="-i"
        # fi

        cp docker-compose-template.yaml docker-compose.yaml


    CURRENT_DIR=$PWD
    cd crypto-config/peerOrganizations/upc.uniconsortium.com/ca/
    PRIV_KEY=$(ls *_sk)
    cd $CURRENT_DIR
    sed $OPTS "s/CA1_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose.yaml

    cd crypto-config/peerOrganizations/ub.uniconsortium.com/ca/
    PRIV_KEY=$(ls *_sk)
    cd $CURRENT_DIR
    sed $OPTS "s/CA2_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose.yaml

    cd crypto-config/peerOrganizations/generalitat.uniconsortium.com/ca/
    PRIV_KEY=$(ls *_sk)
    cd $CURRENT_DIR
    sed $OPTS "s/CA3_PRIVATE_KEY/${PRIV_KEY}/g" docker-compose.yaml

}

function generateCerts(){

    echo
        echo "##########################################################"
        echo "##### Generate certificates using cryptogen tool #########"
        echo "##########################################################"
        if [ -d ./crypto-config ]; then
                rm -rf ./crypto-config
        fi

    cryptogen generate --config=./crypto-config.yaml
    echo
}


function generateChannelArtifacts(){

    if [ ! -d ./channel-artifacts ]; then
                mkdir channel-artifacts
        fi

    echo
        echo "##########################################################"
        echo "#########  Generating Orderer Genesis block ##############"
        echo "##########################################################"
    configtxgen -profile UniOrdererGenesis -channelID testchainid -outputBlock ./channel-artifacts/genesis.block

    echo
        echo "###########################################################################"
        echo "##### Generating channel configuration transaction 'commonChannel.tx' #####"
        echo "###########################################################################"
    configtxgen -profile commonchannel -outputCreateChannelTx ./channel-artifacts/commonChannel.tx -channelID "commonchannel"

    echo
        echo "###########################################################################"
        echo "##### Generating channel configuration transaction 'upcGenChannel.tx' #####"
        echo "###########################################################################"
    configtxgen -profile upcgenchannel -outputCreateChannelTx ./channel-artifacts/upcGenChannel.tx -channelID "upcgenchannel"

    echo
        echo "###########################################################################"
        echo "##### Generating channel configuration transaction 'ubGenChannel.tx' #####"
        echo "###########################################################################"
    configtxgen -profile ubgenchannel -outputCreateChannelTx ./channel-artifacts/ubGenChannel.tx -channelID "ubgenchannel"
    echo

}

function startNetwork() {

    echo
    echo "================================================="
    echo "---------- Starting the network -----------------"
    echo "================================================="
    echo

    cd $PROJECT_DIR
    docker-compose -f docker-compose.yaml up -d
}

function cleanNetwork() {
    cd $PROJECT_DIR

    if [ -d ./channel-artifacts ]; then
            rm -rf ./channel-artifacts
    fi

    if [ -d ./crypto-config ]; then
            rm -rf ./crypto-config
    fi

    if [ -d ./tools ]; then
            rm -rf ./tools
    fi

    if [ -f ./docker-compose.yaml ]; then
        rm ./docker-compose.yaml
    fi

    if [ -f ./docker-compose.yaml ]; then
        rm ./docker-compose.yaml
    fi

    # This operations removes all docker containers and images regardless
    #
    docker rm -f $(docker ps -aq)

    # This removes containers used to support the running chaincode.
    #docker rm -f $(docker ps --filter "name=dev" --filter "name=peer0.upc.uniconsortium.com" --filter "name=cli" --filter "name=orderer.uniconsortium.com" -q)

    # This removes only images hosting a running chaincode, and in this
    # particular case has the prefix dev-*
    #docker rmi $(docker images | grep dev | xargs -n 1 docker images --format "{{.ID}}" | xargs -n 1 docker rmi -f)
}

function networkStatus() {
    docker ps --format "{{.Names}}: {{.Status}}" | grep '[peer0* | orderer* | cli ]'
}

function dockerCli(){
    docker exec -it cli /bin/bash
}

# Network operations
verifyArg
case $COMMAND in
    "start")
        generateCerts
        generateChannelArtifacts
        replacePrivateKey
        pullDockerImages
        startNetwork
        ;;
    "status")
        networkStatus
        ;;
    "clean")
        cleanNetwork
        ;;
    "cli")
        dockerCli
        ;;
    *)
        echo "Useage: networkOps.sh start | status | clean | cli "
        exit 1;
esac