# Required tools to operate an Hyperledger Fabric network

This is a simple guide to install the required network tools. Please execute these commands as `root`.

You will need docker and docker-compose, the following instructions are to install them in Ubuntu 18.04 LTS:

```sh
# Install docker
apt install -y docker.io

# Check that you have successfully installed docker by executing:
docker --version

# Should print the following output:
# Docker version 18.09.7, build 2d0083d

# Install docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Check that you have successfully installed docker-compose by executing:
docker-compose --version

# Should print the following output:
# docker-compose version 1.24.1, build 4667896b
```

You will aso need binaries of the Hyperledger Fabric components:

```sh
wget https://github.com/hyperledger/fabric/releases/download/v2.1.0/hyperledger-fabric-linux-amd64-2.1.0.tar.gz

tar -xzf hyperledger-fabric-linux-amd64-2.1.0.tar.gz

mv bin/* /bin

# Check that you have successfully installed the tools by executing
configtxgen --version

# Should print the following output:
# configtxgen:
#  Version: 2.1.0
#  Commit SHA: 1bdf975
#  Go version: go1.14.1
#  OS/Arch: linux/amd64
```