package main

import (
	"github.com/hyperledger/fabric-chaincode-go/shim"
	peer "github.com/hyperledger/fabric-protos-go/peer"
)

// Invoke is called per transaction on the chaincode. Each transaction is
// either a 'get' or a 'set' on the degree created by Init function. The 'set'
// method may create a new degree by specifying a new key-value pair.

func (a *degree) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	// As with the Init function above, we need to extract the arguments from
	// the ChaincodeStubInterface.
	// The Invoke function’s arguments will be the name of the chaincode application
	// function to invoke.
    // In our case, our application will simply have two functions: set and get,
    // that allow the value of an degree to be set or its current state to be retrieved.
    // We first call ChaincodeStubInterface.GetFunctionAndParameters
	// to extract the function name and the parameters to that chaincode
	// application function.

	// Extract the function and args from the transaction proposal
	// GetFunctionAndParameters returns the first argument as the function
	// name and the rest of the arguments as parameters in a string array.
	// Only use GetFunctionAndParameters if the client passes arguments intended
	// to be used as strings.
	function, args := stub.GetFunctionAndParameters()

    // Next, we’ll validate the function name, and invoke those
	// chaincode application functions, returning an appropriate response via
	//  the shim.Success or shim.Error functions
	// that will serialize the response into a gRPC protobuf message

	var err error

	switch function {
	case "initDegree":
		return a.initDegree(stub, args)
	case "readDegree":
		return a.readDegree(stub, args)
	case "deleteDegree":
		return a.deleteDegree(stub, args)
	case "changeOwnerDegree":
		return a.changeOwnerDegree(stub, args)
	default:
		return shim.Error("Chaincode Degree - Service_invoke - Invalid invoke function name: " + function + err.Error())
	}

	return shim.Error("Received unknown function invocation")

}
