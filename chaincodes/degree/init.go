package main

// Import the shim package which enables interaction with the peer and ledger
import (
	"fmt"
	"github.com/hyperledger/fabric-chaincode-go/shim"
    peer "github.com/hyperledger/fabric-protos-go/peer"
)

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

// Degree struct defines the properties of the Degree that wants 
// to write on the ledger
// Name: Degree title
// University: The university that this degree is issued from
// Date: Issuance date
// Owner: The student that receives the title
type degree struct {
	Name		string	`json:"name"`
	University	string	`json:"university"`
	Date		string	`json:"date"`
	Owner		string	`json:"owner"`
}

// Init is called during chaincode instantiation to initialize any
// data. Note that chaincode upgrade also calls this function to reset
// or to migrate data, so be careful to avoid a scenario where you
// inadvertently clobber your ledger's data!

func (a *degree) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

// Main function starts up the chaincode in the container during instantiate
func main() {
	err := shim.Start(new(degree))
	if err != nil {
		fmt.Printf("Error starting Degree chaincode: %s", err)
	}
}
