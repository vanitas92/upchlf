package main

import (
    "fmt"
    "strconv"
    "strings"
    "encoding/json"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	peer "github.com/hyperledger/fabric-protos-go/peer"
)

// ============================================================
// initDegree - create a new degree, store into chaincode state
// ============================================================

func (a *degree) initDegree(stub shim.ChaincodeStubInterface, args []string) peer.Response {
    var err error

    if len(args) != 4 {
        return shim.Error("Incorrect number of arguments. Expecting 4")
    }

    fmt.Println("- start init Degree function")

    if len(args[0]) <= 0 {
        return shim.Error("1st argument must be a non-empty string")
    } else if len(args[1]) <= 0 {
        return shim.Error("2nd argument must be a non-empty string")
    } else if len(args[2]) <= 0 {
        return shim.Error("3rd argument must be a non-empty string")
    } else if len(args[3]) <= 0 {
        return shim.Error("4rd argument must be a non-empty string")
    }

    degreeName := args[0]
    university := strings.ToLower(args[1])
    owner := strings.ToLower(args[2])
    date, err := strconv.Atoi(args[3])
    if err != nil {
        return shim.Error("4rd argument, date, must be a numeric string")
    }

    // ==== Check if degree already exists ====
    degreeAsBytes, err := stub.GetState(degreeName)
    if err != nil {
        return shim.Error("Failed to get Degree: " + err.Error())
    } else if degreeAsBytes != nil {
        fmt.Println("This Degree already exists: "+ degreeName)
        return shim.Error("This Degree already exists: " + degreeName)
    }

	// ==== Create Degree object and marshal to JSON ====
    degree := &degree{degreeName, university, strconv.Itoa(date), owner}
    degreeJSONasBytes, err := json.Marshal(degree)
    if err != nil {
        return shim.Error(err.Error())
    }

    // ==== Save degree to state ====
    err = stub.PutState(degreeName, degreeJSONasBytes)
    if err != nil {
        return shim.Error(err.Error())
    }

    return shim.Success(nil)
}


// ===============================================
// readDegree - read a degree from chaincode state
// ===============================================

func (a *degree) readDegree(stub shim.ChaincodeStubInterface, args []string) peer.Response {
    var name, jsonResp string
    var err error

    if len(args) != 1 {
        return shim.Error("Incorrect number of arguments. Expecting name of the degree to query")
    }

    name = args[0]
    valAsBytes, err := stub.GetState(name) // get the degree from chaincode state
    if err != nil {
        jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
        return shim.Error(jsonResp)
    } else if valAsBytes == nil {
        jsonResp = "{\"Error\":\"Degree does not exist: " + name + "\"}"
        return shim.Error(jsonResp)
    }

    return shim.Success(valAsBytes)
}

// ==================================================
// delete - remove an degree key/value pair from state
// ==================================================

func (a *degree) deleteDegree(stub shim.ChaincodeStubInterface, args []string) peer.Response {
    var jsonResp string
    var degreeJSON degree

    if len(args) != 1 {
        return shim.Error("Incorrect number of arguments. Expecting 1")
    }

    degreeName := args[0]

    valAsBytes, err := stub.GetState(degreeName)

    if err != nil {
        jsonResp = "{\"Error\":\"Failed to get state for " + degreeName + "\"}"
		return shim.Error(jsonResp)
	} else if valAsBytes == nil {
		jsonResp = "{\"Error\":\"Degree does not exist: " + degreeName + "\"}"
		return shim.Error(jsonResp)
    }

    err = json.Unmarshal(valAsBytes, &degreeJSON)
    if err != nil {
		jsonResp = "{\"Error\":\"Failed to decode JSON of: " + degreeName + "\"}"
		return shim.Error(jsonResp)
    }

	err = stub.DelState(degreeName) //remove the degree from chaincode state
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
    }

	return shim.Success(nil)

}

// ================================================================
// changeOwnerDegree - change an degree owner/value pair from state
// ================================================================

func (a *degree) changeOwnerDegree(stub shim.ChaincodeStubInterface, args []string) peer.Response {
    if len(args) < 2 {
        return shim.Error("Incorrect number of arguments. Expecting 2")
    }

    degreeName := args[0]
    newOwner := strings.ToLower(args[1])

    degreeAsBytes, err := stub.GetState(degreeName)
    if err != nil {
        return shim.Error("Failed to get degree: " + err.Error())
    } else if degreeAsBytes == nil {
        return shim.Error("Degree does not exist")
    }

    degreeToModify := degree{}
    err = json.Unmarshal(degreeAsBytes, &degreeToModify) // unmarshal it is the same as JSON.parse
    if err != nil {
        return shim.Error(err.Error())
    }

    degreeToModify.Owner = newOwner //set the new owner

    degreeJSONasBytes, _ := json.Marshal(degreeToModify)
    err = stub.PutState(degreeName, degreeJSONasBytes) // rewrite the degree with the new owner
    if err != nil {
        return shim.Error(err.Error())
    }

    return shim.Success(nil)
}
