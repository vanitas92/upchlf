# Code Template for bootstraping an Hyperledger Fabric Network

Please follow the following steps to get the network up and running:

1. Follow the `setup` Readme file to get the necessary files and tools needed in this workshop.
2. Modify `configtx.yaml`, `crypto-config.yaml` and `docker-compose-template.yaml` to configure your network.
3. Once done, execute `./fabricOps.sh start` to bootstrap the network.
4. Check everything is fine with `docker ps`.
5. For operating the network, use `./fabricOps.sh cli` and use the tools available in there.
6. To shutdown the network, use `./fabricOps.sh clean`.

## Useful Commands

### Get list of chaincodes installed on the peer

```
peer lifecycle chaincode queryinstalled
```

### Get list of chaincodes committed on a channel

```
peer lifecycle chaincode querycommitted -C upcgenchannel
```

### Check the commit readiness on some chaincode

```
peer lifecycle chaincode checkcommitreadiness --channelID upcgenchannel --name degree --version 1.0 --init-required --sequence 1 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem
```

### Invoke to modify some ledger information

```
peer chaincode invoke --tls true -C upcgenchannel -n degree --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/uniconsortium.com/orderers/orderer.uniconsortium.com/msp/tlscacerts/tlsca.uniconsortium.com-cert.pem -c '{"Args":["initDegree","telecom","upc","pau","03071992"]}' --peerAddresses peer0.generalitat.uniconsortium.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/generalitat.uniconsortium.com/peers/peer0.generalitat.uniconsortium.com/tls/ca.crt --peerAddresses peer0.upc.uniconsortium.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/upc.uniconsortium.com/peers/peer0.upc.uniconsortium.com/tls/ca.crt
```

### Query some loedger information

```
peer chaincode query -C upcgenchannel -n degree -c '{"Args":["readDegree","telecom"]}'
```